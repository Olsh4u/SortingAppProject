package com.olsh4u.epam.model;


import java.util.Arrays;

public class BubbleSorter implements Sorter {
    @Override
    public String sort(int[] arr) {
        if (arr.length == 0) throw new IllegalArgumentException("0 arguments passed");
        if (arr.length == 1) throw new IllegalArgumentException("only 1 argument passed");
        if (arr.length > 10) throw new IllegalArgumentException("more than 10 arguments passed");
        int n = arr.length;
        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - i - 1; j++) {
                if (arr[j] > arr[j + 1]) {
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }
        return Arrays.toString(arr);
    }
}
