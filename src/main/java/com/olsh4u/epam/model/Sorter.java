package com.olsh4u.epam.model;

public interface Sorter {
    String sort(int[] arr);
}
