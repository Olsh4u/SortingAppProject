package com.olsh4u.epam;

import com.olsh4u.epam.controller.NumberSorter;
import com.olsh4u.epam.model.BubbleSorter;
import com.olsh4u.epam.model.Sorter;

import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("enter up to 10 ints splitting them with spacebar");
        Scanner in = new Scanner(System.in);
        String input = in.nextLine();
        Sorter sorter = new BubbleSorter();

        String[] items = input.split(" ");

        int[] arguments = new int[items.length];

        for (int i = 0; i < items.length; i++) arguments[i] = Integer.parseInt(items[i]);

        sorter.sort(arguments);

        System.out.println(Arrays.toString(arguments));

    }
}
