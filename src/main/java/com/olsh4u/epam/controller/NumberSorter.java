package com.olsh4u.epam.controller;

import com.olsh4u.epam.model.Sorter;

public class NumberSorter{
    private final Sorter sorter;

    public NumberSorter(Sorter sorter) {
        this.sorter = sorter;
    }

    public void sortNumbers(int[] arr) {
        sorter.sort(arr);
    }
}
