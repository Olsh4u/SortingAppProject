package com.olsh4u.epam.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;


@RunWith(Parameterized.class)
public class BubbleSorterTest {
    protected Sorter sortingApp = new BubbleSorter();

    private final int[] arrayTest;
    private final String expected;

    public BubbleSorterTest (int[] arrayTest, String expected){
        this.arrayTest = arrayTest;
        this.expected = expected;
    }

    @Test(expected = IllegalArgumentException.class)
    public void testZeroArgsCase(){
        int[] zeroArgsArray = new int[0];
        sortingApp.sort(zeroArgsArray);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testOneArgCase(){
        int[] oneArgArray = new int[1];
        sortingApp.sort(oneArgArray);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMoreThanTenArgsCase(){
        int[] zeroArray = new int[11];
        sortingApp.sort(zeroArray);
    }

    @Test
    public void testTwoToTenAgrsCase(){
        assertEquals(expected, sortingApp.sort(arrayTest));
    }

    @Parameterized.Parameters
    public static Collection input(){
        int[] array1 = {1, -2},
                array2 = {100, 41, -5, 0},
                array3 = {555, 45, -1, 1, 0, 10},
                array4 = {9, 5, 4, 1, 2, 3, 7, 6, 10, 8};
        return Arrays.asList(new Object[][]{
                {array1, "[-2, 1]"},
                {array2, "[-5, 0, 41, 100]"},
                {array3, "[-1, 0, 1, 10, 45, 555]"},
                {array4, "[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]"}});
    }
}
